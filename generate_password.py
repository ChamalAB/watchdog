import getpass
from werkzeug.security import generate_password_hash

info = "Use this tool to generate a pbkdf password to the app\n\n"
print(info)
pswd = getpass.getpass('Password:')
print(generate_password_hash(pswd))