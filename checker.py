import time
import requests

url = 'https://watchdog.thenxtworld.com'

data = {
    'name':'sys',
    'token':'K463C4O8IDZG2F886I0WUSKGQAE04CY2',
}

interval = 60 # 4 minutes


def send_check(url,data,interval):
    while True:
        try:
            time.sleep(interval)
            r = requests.get(url + '/api/check', params=data)
            if r.status_code != 200:
                raise Exception("Request failed code: {}".format(r.status_code))

        except KeyboardInterrupt:
            print('exiting...')
            break
        except Exception as e:
            print(str(e))
            
if __name__ == '__main__':
    send_check(url,data,interval)