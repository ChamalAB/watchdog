
import os
import datetime

from flask import Flask, session, redirect, render_template, url_for, abort

import watchdog.Server.Database
from watchdog.Server.Auth import authHandler, auth
from watchdog.Server.Puppy import puppy
from watchdog.Server.Log import log
from watchdog.Server.Task import task
from watchdog.Server.Api import api
from watchdog.Utils import grabconfig, config_data

app = Flask(__name__)
app.register_blueprint(auth, url_prefix='/auth')
app.register_blueprint(puppy, url_prefix='/puppy')
app.register_blueprint(log, url_prefix='/log')
app.register_blueprint(task, url_prefix='/task')
app.register_blueprint(api, url_prefix='/api')


showConfigs = grabconfig('showConfigs', 'showConfigs', object_type=int)
app.secret_key = os.urandom(16)

# inject time
time_now = lambda : str(datetime.datetime.now())[:19]

@app.context_processor
def inject_today_date():
    return {'today_date': time_now()}


@app.route('/')
@app.route('/index')
def index():
    return redirect(url_for('admin'))


@app.route('/admin')
@authHandler
def admin():
    return render_template('admin.html')

@app.route('/configs')
def configs():
	if showConfigs:
		return render_template('configs.html', configs=config_data)
	else:
		abort(404)