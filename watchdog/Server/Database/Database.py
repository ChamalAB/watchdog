# standard
import time
import sqlite3


# third-party

# local
from .db_init import DB_DDL_INIT, DB_DML_INIT, SQLITE3_DB_FILENAME



def connection(func):
    """
    Decorator for database connection handling
    """
    def wrapper(*args, **kwargs):
        kwargs['conn'] = sqlite3.connect(SQLITE3_DB_FILENAME)
        ret = func(*args, **kwargs)
        kwargs['conn'].close()
        return ret
    return wrapper




def dictFactory(func):
    """
    adds dictionaries format for row_factory
    Returns a dict object with results
    Use only with query statements
    """
    def wrapper(*args, **kwargs):
        def dict_factory(cursor, row):
            d = {}
            for idx, col in enumerate(cursor.description):
                d[col[0]] = row[idx]
            return d
        kwargs['conn'].row_factory = dict_factory
        return [row for row in func(*args, **kwargs)]
    return wrapper




# create database and intialize with default data
@connection
def initialize_db(*args, **kwargs):
    kwargs['conn'].executescript(DB_DDL_INIT)
    kwargs['conn'].executescript(DB_DML_INIT)




@connection
def createTask(Puppy_Name, LogLevel_idLevel, Job,  **kwargs):
    conn = kwargs['conn']
    sql = 'INSERT OR REPLACE INTO Task (Puppy_Name, LogLevel_idLevel, Job) VALUES (?,?,?)'
    params = (Puppy_Name, LogLevel_idLevel, Job)
    conn.execute(sql,params)
    conn.commit()






# test code
initialize_db()
# createPuppy('Chamal','PSST','POST')
# createLog('Chamal','INFO','JUST A LOG')
# createTask('Chamal','INFO','SEND EMAIL')
# x = getPuppy('Chamal', Logs=False)
# print(x)
