from watchdog.Utils import grabconfig

SQLITE3_DB_FILENAME = grabconfig('database','name')

DB_DDL_INIT = """
CREATE TABLE IF NOT EXISTS "Puppy" (
	"Name"	VARCHAR(32) NOT NULL,
	"Token"	VARCHAR(32) NOT NULL,
	"Method"	VARCHAR(8) NOT NULL,
	"ReportInterval"	INT DEFAULT 3600,
	"ReportLast"	INT DEFAULT 0,
	"NotifyInterval"	INT DEFAULT 900,
	"NotifyLast"	INT DEFAULT 0,
	PRIMARY KEY("Name")
);
CREATE TABLE IF NOT EXISTS "LogLevel" (
	"idLevel"	VARCHAR(8) NOT NULL,
	PRIMARY KEY("idLevel")
);
CREATE TABLE IF NOT EXISTS "Log" (
	"idLog"	INTEGER NOT NULL,
	"Puppy_Name"	VARCHAR(32) NOT NULL,
	"LogLevel_idLevel"	VARCHAR(8) NOT NULL,
	"Timestamp"	INT NOT NULL,
	"Message"	VARCHAR(1024),
	CONSTRAINT "fk_Log_Puppy1" FOREIGN KEY("Puppy_Name") REFERENCES "Puppy"("Name") ON DELETE NO ACTION ON UPDATE NO ACTION,
	PRIMARY KEY("idLog" AUTOINCREMENT),
	CONSTRAINT "fk_Log_LogLevel" FOREIGN KEY("LogLevel_idLevel") REFERENCES "LogLevel"("idLevel") ON DELETE NO ACTION ON UPDATE NO ACTION
);
CREATE TABLE IF NOT EXISTS "Task" (
	"idTask"	INTEGER NOT NULL,
	"Puppy_Name"	VARCHAR(32) NOT NULL,
	"LogLevel_idLevel"	VARCHAR(8) NOT NULL,
	"Job"	VARCHAR(1024) NOT NULL,
	CONSTRAINT "fk_Task_Puppy1" FOREIGN KEY("Puppy_Name") REFERENCES "Puppy"("Name") ON DELETE NO ACTION ON UPDATE NO ACTION,
	PRIMARY KEY("idTask" AUTOINCREMENT),
	CONSTRAINT "fk_Task_LogLevel1" FOREIGN KEY("LogLevel_idLevel") REFERENCES "LogLevel"("idLevel") ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE IF NOT EXISTS "User" (
	"Name"	VARCHAR(16) NOT NULL,
	"Hash"	VARCHAR(256) NOT NULL,
	PRIMARY KEY("Name")
);
"""

DB_DML_INIT = """
INSERT OR IGNORE INTO
LogLevel(idLevel)
VALUES
('DEBUG'),
('INFO'),
('WARNING'),
('ERROR')
;

INSERT OR REPLACE INTO
Puppy(Name,Token,Method)
VALUES
('{Name}','{Token}','{Method}')
;

INSERT OR REPLACE INTO
User(Name,Hash)
VALUES
('{UName}','{UHash}')
;
""".format(
    Name=grabconfig('sys','Name'),
    Token=grabconfig('sys','Token'),
    Method=grabconfig('sys','Method'),
    UName=grabconfig('UserRoot','rootUsername'),
    UHash=grabconfig('UserRoot','rootHash')
)
