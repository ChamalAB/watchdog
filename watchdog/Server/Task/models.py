import time

from watchdog.Server.Database import connection, dictFactory


@connection
def createTask(Puppy_Name, LogLevel_idLevel, Job,  **kwargs):
    result = 'OK'
    try:
        conn = kwargs['conn']
        sql = 'INSERT INTO Task (Puppy_Name, LogLevel_idLevel, Job) VALUES (?,?,?)'
        params = (Puppy_Name, LogLevel_idLevel, Job)
        conn.execute(sql,params)
        conn.commit()
    except Exception as e:
        result = str(e)
    finally:
        return result


@connection
def deleteTask(idTask, **kwargs):
    result = 'OK'
    try:
        conn = kwargs['conn']
        sql = 'DELETE FROM Task WHERE idTask = ?'
        params = (idTask,)
        conn.execute(sql,params)
        conn.commit()
    except Exception as e:
        result = str(e)
    finally:
        return result


@connection
@dictFactory
def getTasks(*args, **kwargs):
    return kwargs['conn'].execute('select * from Task')


@connection
@dictFactory
def getTask(Puppy_Name, *args, **kwargs):
    return kwargs['conn'].execute('select * from Task WHERE Puppy_Name=?', (Puppy_Name,))