

from flask import session, redirect, Blueprint, render_template, request, jsonify, abort


from watchdog.Server.Auth import authHandler
from .models import createTask, getTask, getTasks, deleteTask
from  watchdog.Utils import check_bad_request


task = Blueprint('task', __name__, template_folder='templates')


@task.route('/', methods=['GET', 'POST'])
@authHandler
def task_get_set():
	if request.method == 'GET':
		if request.args.get('name', None):
			Name = request.args.get('name')
			return jsonify(getTask(Name))
		else:
			return jsonify(getTasks())

	elif request.method == 'POST':
		# check for form contents
		Name = request.form.get('PuppyName',None)
		Level = request.form.get('LogLevel',None)
		Job = request.form.get('Job',None)
		check_bad_request(Name, Level, Job)
		result = createTask(Name, Level, Job)
		return render_template('message.html', message=result)

	else:
		abort(405)


@task.route('/delete', methods=['GET', 'POST'])
@authHandler
def task_del():
	if request.method == 'GET':
		return render_template('delete_task.html')
	else:
		task_id = request.form.get('taskId',None)
		check_bad_request(task_id)
		result = deleteTask(task_id)
		return render_template('message.html', message=result)


@task.route('/create', methods=['GET'])
@authHandler
def create_task_page():
	return render_template('create_task.html')