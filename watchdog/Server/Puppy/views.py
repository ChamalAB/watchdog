

from flask import session, redirect, Blueprint, render_template, request, jsonify, abort


from watchdog.Server.Auth import authHandler
from .models import createPuppy, getPuppies, getPuppy, deletePuppy
from  watchdog.Utils import check_bad_request

puppy = Blueprint('puppy', __name__, template_folder='templates')



@puppy.route('/', methods=['GET', 'POST'])
@authHandler
def puppy_get_set():
	if request.method == 'GET':
		if request.args.get('name', None):
			Name = request.args.get('name')
			return jsonify(getPuppy(Name, Tasks=True, Logs=True))
		else:
			return jsonify(getPuppies())

	elif request.method == 'POST':
		# check for form contents
		Name = request.form.get('PuppyName',None)
		Token = request.form.get('PuppyToken',None)
		Method = request.form.get('PuppyMethod',None)
		NotifyInterval = request.form.get('PuppyNotPer',None)
		check_bad_request(Name, Token, Method, NotifyInterval)
		result = createPuppy(Name, Token, Method, NotifyInterval)
		return render_template('message.html', message=result)
	else:
		abort(405)


@puppy.route('/delete', methods=['GET','POST'])
@authHandler
def puppy_del():
	if request.method == 'GET':
		return render_template('delete_puppy.html')
	else:
		Name = request.form.get('name',None)
		check_bad_request(Name)
		result = deletePuppy(Name)
		return render_template('message.html', message=result)

@puppy.route('/create', methods=['GET'])
@authHandler
def create_puppy_page():
	return render_template('create_puppy.html')