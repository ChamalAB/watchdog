

from watchdog.Server.Database import connection, dictFactory


# create functions
@connection
def createPuppy(Name, Token, Method, NotifyInterval, **kwargs):
    result = 'OK'
    try:
        # run simple validation
        if not Method in ['GET', 'POST'] : raise ValueError("Invalid Method: {}".format(Method))

        conn = kwargs['conn']
        sql = 'INSERT OR REPLACE INTO Puppy (Name, Token, Method, NotifyInterval) VALUES (?,?,?,?)'
        params = (Name, Token, Method, NotifyInterval)
        conn.execute(sql,params)
        conn.commit()
    except Exception as e:
        result = str(e)
    finally:
        return result

# create functions
@connection
def updatePuppy(Name, ReportInterval, ReportLast, NotifyLast, **kwargs):
    """
    Thos function is use specifically to update report and notfiy columnts
    """
    result = 'OK'
    try:
        conn = kwargs['conn']
        sql = 'UPDATE Puppy SET ReportInterval = ? , ReportLast = ? , NotifyLast = ? WHERE Name = ?'
        params = (ReportInterval, ReportLast, NotifyLast, Name)
        conn.execute(sql,params)
        conn.commit()
    except Exception as e:
        result = str(e)
    finally:
        return result

@connection
def deletePuppy(Name, **kwargs):
    result = 'OK'
    try:
        conn = kwargs['conn']
        sql = 'DELETE FROM Puppy WHERE Name = ?'
        params = (Name,)
        conn.execute(sql,params)
        conn.commit()
    except Exception as e:
        result = str(e)
    finally:
        return result


@connection
@dictFactory
def getPuppies(*args, **kwargs):
    return kwargs['conn'].execute('select * from Puppy')


@connection
@dictFactory
def getPuppy(name, Tasks=True, Logs=False, **kwargs):
    conn = kwargs['conn']
    # a list is create becuase 
    puppy = {}
    # get puppy with name
    for pup in conn.execute('SELECT * FROM Puppy WHERE Name = ?', (name,)):
        puppy = pup

        # get tasks by puppy name
        if Tasks:
            puppy['Tasks'] = []
            for task in conn.execute('SELECT * FROM Task WHERE Puppy_Name = ?', (name,)):
                puppy['Tasks'].append(task)

        # get logs by puppy name
        if Logs:
            puppy['Logs'] = []
            for log in conn.execute('SELECT * FROM Log WHERE Puppy_Name = ?', (name,)):
                puppy['Logs'].append(log)

    # object is placed in to a list to safely execute @dictFacotry decorator without error
    return [puppy]