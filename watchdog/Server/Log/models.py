import time
import re

from watchdog.Server.Database import connection, dictFactory


@connection
def createLog(Puppy_Name, LogLevel_idLevel, Message,  **kwargs):
    result = 'OK'
    try:
        conn = kwargs['conn']
        sql = 'INSERT INTO Log (Puppy_Name, LogLevel_idLevel, Timestamp, Message) VALUES (?,?,?,?)'
        params = (Puppy_Name, LogLevel_idLevel, int(time.time()), Message)
        conn.execute(sql,params)
        conn.commit()
    except Exception as e:
        result = str(e)
    finally:
        return result


@connection
def deleteLog(idLog, **kwargs):
    result = 'OK'
    try:
        conn = kwargs['conn']
        sql = gen_delete_sql('Log','idLog',idLog)
        conn.execute(sql)
        conn.commit()
    except Exception as e:
        result = str(e)
    finally:
        return result


@connection
@dictFactory
def getLogs(*args, **kwargs):
    return kwargs['conn'].execute('select * from Log')


@connection
@dictFactory
def getLog(Puppy_Name, *args, **kwargs):
    return kwargs['conn'].execute('select * from Log WHERE Puppy_Name=?', (Puppy_Name,))


def get_match(pattern,text):
    a = re.search(pattern,text)
    if a:
        return a.group()
    else:
        return None

def gen_delete_sql(tableName, column, in_text):
    in_text = in_text.replace(" ","")
    sql_prefix = 'DELETE FROM {table} WHERE '.format(table=tableName)
    condition = ''
    
    empty = "^(?![\s\S])" # for empty
    single = "^\d+$" # single values
    num_list = "^\d+(,{1}\d+)+$" # list of numbers
    between = "^\d+-{1}\d+$" # between
    comparative = "^[<>]{1}\d+" # comparative
    
    # ignore emty strings (safety check lol)
    if get_match(empty,in_text):
        return None
    
    a = get_match(single,in_text)
    b = get_match(num_list,in_text)
    c = get_match(between,in_text)
    d = get_match(comparative,in_text)
    # get in values
    if a or b:
        condition =  '{column} IN ({condition})'.format(column=column, condition= a or b)
    elif c:
        num1, num2 = c.split('-')
        condition =  '{column} >= {num1} AND {column} <= {num2}'.format(column=column, num1=num1, num2=num2)
    elif d:
        num = d[1:]
        operator = d[:1]
        condition =  '{column} {operator} {num}'.format(column=column, operator=operator, num=num)
    else:
        return None
    
    return sql_prefix + condition