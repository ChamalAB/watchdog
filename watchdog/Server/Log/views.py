

from flask import session, redirect, Blueprint, render_template, request, jsonify, abort


from watchdog.Server.Auth import authHandler
from .models import createLog, getLog, getLogs, deleteLog
from  watchdog.Utils import check_bad_request


log = Blueprint('log', __name__, template_folder='templates')


@log.route('/', methods=['GET', 'POST'])
@authHandler
def log_get_set():
	if request.method == 'GET':
		if request.args.get('name', None):
			Name = request.args.get('name')
			return jsonify(getLog(Name))
		else:
			return jsonify(getLogs())

	elif request.method == 'POST':
		# check for form contents
		Name = request.form.get('PuppyName',None)
		Level = request.form.get('LogLevel',None)
		Message = request.form.get('Message',None)
		check_bad_request(Name, Level, Message)
		result =  createLog(Name, Level, Message)
		return render_template('message.html', message=result)

	else:
		abort(405)


@log.route('/delete', methods=['GET', 'POST'])
@authHandler
def log_del():
	if request.method == 'GET':
		return render_template('delete_log.html')
	else:
		log_id = request.form.get('logId',None)
		check_bad_request(log_id)
		result = deleteLog(log_id)
		return render_template('message.html', message=result)

@log.route('/create', methods=['GET'])
@authHandler
def create_log_page():
	return render_template('create_log.html')