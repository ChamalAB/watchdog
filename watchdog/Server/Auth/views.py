from functools import wraps


from flask import session, redirect, Blueprint, render_template, request, abort, url_for
from werkzeug.security import check_password_hash

from .models import getUser

auth = Blueprint('auth', __name__, template_folder='templates')



def authHandler(func):
	@wraps(func)
	def wrapper(*args, **kwargs):
		if session.get('logged_in'):
			return func(*args, **kwargs)
		else:
			return redirect(url_for('auth.login'))
	return wrapper



@auth.route('/login', methods=['GET', 'POST'])
def login():
	if request.method == 'GET':
		return render_template('login.html')
	else:
		Username = request.form.get("Username",'None')
		Password = request.form.get("Password",'None')

		res = getUser(Username)

		if len(res)==1:
			if res[0]['Name']==Username:
				if check_password_hash(res[0]['Hash'], Password):
					session['logged_in'] = True
					return redirect('/admin')

		session['logged_in'] = False
		return redirect(url_for('auth.login'))


@auth.route('/logout', methods=['GET'])
def logout():
	session['logged_in'] = False
	return redirect(url_for('auth.login'))