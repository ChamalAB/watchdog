

from watchdog.Server.Database import connection, dictFactory


# view functions
@connection
@dictFactory
def getUser(username,*args, **kwargs):
    return kwargs['conn'].execute('select * from User WHERE Name=?',(username,))