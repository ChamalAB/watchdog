from functools import wraps


from flask import session, redirect, Blueprint, render_template, request, abort, url_for

from watchdog.Server.Puppy import getPuppy
from  watchdog.Utils import check_bad_request
from .models import runTasks, runReport, checkReports

api = Blueprint('api', __name__, template_folder='templates')


def apiAuthHandler(func):
	@wraps(func)
	def wrapper(*args, **kwargs):
		# check for bad requests and get data
		name = request.args.get('name', None) or request.form.get('name', None)
		token = request.args.get('token', None) or request.form.get('token', None)

		check_bad_request(name, token)

		# get puppy data
		puppy = getPuppy(name, Tasks=True)

		# check name
		try:
			if puppy[0]['Name'] != name:
				abort(401)
		except KeyError:
			abort(401)

		# check method
		if puppy[0]['Method'].upper() != request.method:
			abort(405)

		# check token
		if puppy[0]['Token'] != token:
			abort(401)

		return func(puppy[0], *args, **kwargs)
	return wrapper


@api.route('/report', methods=['GET', 'POST'])
@apiAuthHandler
def report(puppy_obj):
	reportBack = request.args.get('reportBack', None) or request.form.get('reportBack', None)
	check_bad_request(reportBack)

	# implement business logic models.py
	if runReport(puppy_obj, reportBack) == 'OK':
		return 'OK'
	else:
		abort(500)

@api.route('/check', methods=['GET', 'POST'])
@apiAuthHandler
def check_reporting(*args):
	checkReports()
	return 'OK'


@api.route('/log', methods=['GET', 'POST'])
@apiAuthHandler
def log(puppy_obj):
	level = request.args.get('level', None) or request.form.get('level', None)
	message = request.args.get('message', None) or request.form.get('message', None)
	check_bad_request(level, message)

	# implement business logic models.py
	if runTasks(puppy_obj, level, message) == 'OK':
		return 'OK'
	else:
		abort(500)