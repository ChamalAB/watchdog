import time
import requests
import urllib.parse as urlparse

from watchdog.Server.Log import createLog
from watchdog.Server.Task import getTask
from watchdog.Server.Puppy import updatePuppy, getPuppies


def runReport(puppy_obj, reportBack):
    result = 'OK'
    timestamp = int(time.time())
    try:
        # use insert or replace function to update
        updatePuppy(puppy_obj['Name'], reportBack, timestamp, puppy_obj['NotifyLast'])
    except Exception as e:
        createLog('sys', 'ERROR', 'runReport::{}:{}:{}'.format(e, puppy_obj['Name'], timestamp))
        result = 'ERROR'
    finally:
        return result


def checkReports():
    result = 'OK'
    try:
        now = int(time.time())
        for puppy in getPuppies():
            if puppy['Name'] != 'sys':
                # check if a puppy hasnt reported back
                if now > (puppy['ReportInterval'] + puppy['ReportLast']):
                    # unreported puppy is found
                    if now > (puppy['NotifyInterval'] + puppy['NotifyLast']):
                        # get task list
                        puppy['Tasks'] = getTask(puppy['Name'])
                        # trigger an event
                        runTasks(puppy, 'WARNING', 'Is not reporting')
                        # update notification
                        updatePuppy(puppy['Name'], puppy['ReportInterval'], puppy['ReportLast'], now)
    except Exception as e:
        createLog('sys', 'ERROR', 'checkReports::{}'.format(e))
        result = 'ERROR'
    finally:
        return result


def runTasks(puppy_obj, level, message):
    result = 'OK'
    try:
        # get matching tasks by level
        for task in puppy_obj['Tasks']:
            if task['LogLevel_idLevel'] == level.upper():

                # replace text if placeholders exists
                job = task['Job'].replace('{{message}}',message)
                job = job.replace('{{level}}',level)
                job = job.replace('{{name}}',puppy_obj['Name'])
                job = job.split('::')
                
                if job[0].upper() in ['GET', 'POST']:
                    send_http_request(job)
                elif job[0].upper() in ['LOG', 'LOGGER', 'LOGGING']:
                    result = createLog(puppy_obj['Name'], level.upper(), message)
                elif job[0].upper() in ['TELEGRAM', 'MESSAGE']:
                    TelegramBotSend(job[1], job[2], job[3])
    except Exception as e:
        createLog('sys', 'ERROR', 'runTasks::{}:{}:{}:{}'.format(e, puppy_obj['Name'], level, message))
        result = 'ERROR'
    finally:
        return result


def send_http_request(job_data):
    parsed = urlparse.urlparse(job_data[1])
    url = parsed.scheme+'://'+parsed.netloc+parsed.path
    data = query_parse(parsed.query)
    
    if job_data[0].upper() == 'POST':
        result = requests.post(url, data=data)
    else:
        result = requests.get(url, params=data)
        
    if result.status_code != 200:
        raise Exception('Request failed {}'.format(result.status_code))


def query_parse(query):
    result = {}
    for pair in query.split('&'):
        key, value = pair.split('=')
        result[key] = value
    return result


def TelegramBotSend(bot_api_key, chat_id, text):
    url = 'https://api.telegram.org/bot{bot_api_key}/'\
            'sendMessage?chat_id={chat_id}'\
            '&text={text}'.format(bot_api_key=bot_api_key,
                                 chat_id=chat_id,
                                 text=text)
    
    send_http_request(['GET',url])