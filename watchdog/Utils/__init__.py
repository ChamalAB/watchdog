import os
import sys

from configparser import ConfigParser
from flask import abort


def grabconfig(section,name,object_type=str,filename='config.ini'):
    parser = ConfigParser()
    parser.read(filename)
    return object_type(parser.get(section,name))


def check_bad_request(*args):
    for i in args:
        if not i:
            abort(400)


# config text for system info
config_data = """<strong>Python Version</strong>
{version}

<strong>PID</strong>
{pid}

<strong>CWD</strong>
{cwd}

<strong>PATH</strong>
{path}

<strong>ENV</strong>
{env}
""".format(version=sys.version,
          pid=os.getpid(),
          cwd=os.getcwd(),
          path='\n'.join(sys.path),
          env='\n'.join([k+' : '+v for k,v in os.environ.items()])).replace('\n','<br>')